FROM openjdk:8-jre-slim

ARG MAIN_CLASS
ENV MAIN_CLASS ${MAIN_CLASS:-no-main-class-defined}

ENTRYPOINT ["sh", "-c", "exec java -cp \"service-lib/*\" ${MAIN_CLASS} server ${CONFIG}"]

# Add the service itself
ADD target/lib /service-lib
ADD config.yml /service-lib
ARG BUILD_FINAL_NAME
ADD target/${BUILD_FINAL_NAME}.jar /service-lib/
