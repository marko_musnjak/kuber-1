package com.mmusnjak.resources;

import com.codahale.metrics.annotation.Timed;
import com.mmusnjak.api.Status;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

@Path("/status")
@Produces(MediaType.APPLICATION_JSON)
public class StatusResource {
   private final AtomicLong counter = new AtomicLong(0);
   private final String instanceID;

   public StatusResource(String instanceID) {
      this.instanceID = instanceID;
   }

   @GET
   @Timed
   public Status getStatus() {
      Long currentNum = counter.getAndIncrement();
      return new Status(currentNum, getDescription(currentNum));
   }

   private String getDescription(Long num) {
      return String.format("%s : %s", instanceID, num % 2 == 0 ? "Even" : "Odd");
   }
}
