package com.mmusnjak.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

public class Status {

   @Getter private final Long status;
   @Getter private final String description;

   @JsonCreator
   public Status(
      @JsonProperty("status") Long status,
      @JsonProperty("description") String description) {
      this.status = status;
      this.description = description;
   }
}
