package com.mmusnjak;

import com.mmusnjak.resources.StatusResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.util.Random;

public class KuberAppApplication extends Application<KuberAppConfiguration> {

    public static void main(final String[] args) throws Exception {
        new KuberAppApplication().run(args);
    }

    @Override
    public String getName() {
        return "KuberApp";
    }

    @Override
    public void initialize(final Bootstrap<KuberAppConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final KuberAppConfiguration configuration,
                    final Environment environment) {
        Random rand = new Random();
        environment.jersey().register(new StatusResource(String.valueOf(rand.nextInt())));
    }

}
